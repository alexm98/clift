FROM alpine:3.7

RUN apk add --no-cache nodejs python3 bash
RUN npm install xterm --prefix /
RUN pip3 install terminado

RUN addgroup -S tutorialuser && adduser -s /bin/bash -S tutorialuser -G tutorialuser

COPY ./src/app.py /
COPY ./src/index.html /var/www/localhost/htdocs/
COPY ./src/style.css /var/www/localhost/htdocs/
COPY ./src/script.js /var/www/localhost/htdocs/

RUN mkdir /var/www/localhost/htdocs/lessons/
COPY ./src/lessons/ /var/www/localhost/htdocs/lessons/

RUN mkdir /var/www/localhost/htdocs/img/
COPY ./src/img/ /var/www/localhost/htdocs/img/

RUN mkdir /tmp/tests
COPY ./src/tests/ /tmp/tests/
RUN chmod 755 /tmp/tests/*

EXPOSE 80

USER tutorialuser
RUN mkdir /home/tutorialuser/.passedlevels/

ENV PS1='[\u@\h:\w]\$ '

CMD ["/usr/bin/python3", "/app.py"]
