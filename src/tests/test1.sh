#!/bin/bash

if [ "$(pwd)" == "/home/tutorialuser" ];
then
    echo "Congratulations! you have succesfully completed the first lesson!"
    touch /home/tutorialuser/.passedlevels/1;
else
    echo "You're not in the /home/tutorialuser directory!";
fi
