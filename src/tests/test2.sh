#!/bin/bash

echo "$(history | tail -n 1 | cut -d ' ' -f6)"

if [ "$(history | tail -n 1 | cut -d ' ' -f6)" == "ls -l" ];
then
    echo "Congratulations, you have passed the second lesson!";
else
    echo "Please run the ls command!";
fi
