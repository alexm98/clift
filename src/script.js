function initTerminal(){
    terminado.apply(Terminal);

    var term = new Terminal(),
	protocol = (location.protocol === 'https:') ? 'wss://' : 'ws://',
	socketURL = protocol + location.hostname + ((location.port) ? (':' + location.port) : '') + "/websocket";
    sock = new WebSocket(socketURL);

    sock.addEventListener('open', function () {
	term.terminadoAttach(sock);
    });

    /* term.setOption('theme', { background: '#fdf6e3' }); */
    term.open(document.getElementById('terminal'));
    /* term.write('Hello from \x1B[1;3;31mxterm.js\x1B[0m inside a Docker container $ ') */
}

function loadLesson(path_to_lesson){
    var xobj = new XMLHttpRequest();
    
    xobj.overrideMimeType("application/json");
    xobj.open('GET', path_to_lesson, true);
    xobj.onreadystatechange = function () {
	if (xobj.readyState == 4 && xobj.status == "200") {
	    /* console.log(xobj.responseText); */
	    jsondoc = JSON.parse(xobj.responseText);
	    document.getElementById('title').innerHTML = "<h1>" + jsondoc.title + "</h1>";
     	    document.getElementById('shortdesc').innerHTML = "<p>" + jsondoc.shortdesc + "</p>";
	}
    };
    xobj.send(null);
}

initTerminal();
loadLesson('/var/www/localhost/htdocs/lessons/lesson1.json');
